﻿using System;
using Api2.Entities;

namespace Api2.Models
{
    public class Contract : BaseEntity
    {
        public DateTime CreationDate { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public int BankAccountId { get; set; }
        public decimal Payroll { get; set; }
        public double WorkedHours { get; set; }

        public virtual BankAccount BankAccount { get; set; }
    }
}