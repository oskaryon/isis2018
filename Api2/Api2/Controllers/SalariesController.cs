﻿using System.Linq;
using Api2.Data;
using Api2.Entities;
using Api2.Models;
using Api2.YouTrackApi;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Frameworks;
using YouTrackSharp.Issues;

namespace Api2.Controllers
{
    public class SalariesController : Controller
    {
        private readonly MyDbContext _context;

        public SalariesController(MyDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult YtUnloading()
        {
            return View();
        }

        [HttpPost]
        public IActionResult YtUnloading([FromServices] YouTrackHandler ytHandler)
        {
            //return RedirectToAction("Index");

            var allWorkers = ytHandler.ProcessPayment();

            allWorkers.ForEach(x => x.Item1.ForEach(y =>
            {
                var individual = IndividualMapping(y);

                if (IsCEO(individual))
                {
                    var companyContract = _context.CompanyContracts
                        .Include("Company")
                        .FirstOrDefault(z => z.Company.CEOId == individual.Id);
                    //companyContract.Payroll = companyContract.Rate * (decimal) x.Item2;
                    companyContract.WorkedHours = x.Item2;
                }
                else
                {
                    var individualContract = _context.IndividualContracts
                        .Include("Individual")
                        .FirstOrDefault(z => z.IndividualId == individual.Id);
                    //individualContract.Payroll = individualContract.Salary;
                    individualContract.WorkedHours = x.Item2;
                }
            }));
            _context.SaveChanges();

            // Начисление

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Calculate()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Calculate(BaseEntity contract)
        {
            foreach (var companyContract in _context.CompanyContracts)
            {
                companyContract.Payroll = companyContract.Rate * (decimal)companyContract.WorkedHours;
            }

            foreach (var individualContract in _context.IndividualContracts)
            {
                individualContract.Payroll = individualContract.Salary;
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult Payroll()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Payroll(BaseEntity contract)
        {
            

            return RedirectToAction("Index");
        }

        // ReSharper disable once InconsistentNaming
        private bool IsCEO(Individual individual)
        {
            return _context.Companies.Any(x => x.CEOId == individual.Id);
        }

        private Individual IndividualMapping(Assignee assignee)
        {
            return _context.Individuals.FirstOrDefault(x => x.UserName == assignee.UserName);
        }
    }
}