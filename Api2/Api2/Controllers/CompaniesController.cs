﻿using System.Threading.Tasks;
using Api2.Data;
using Api2.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api2.Controllers
{
    public class CompaniesController : CrudController<Company>
    {
        public CompaniesController(MyDbContext context) : base(context)
        {
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override async Task<IActionResult> Create(Company company)
        {
            if (ModelState.IsValid)
            {
                company.CEO = await Context.Individuals.FindAsync(company.CEOId);
                await DbSet.AddRangeAsync(company);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public override async Task<IActionResult> Index()
        {
            return View(await Context.Companies.Include("CEO").ToListAsync());
        }
    }
}