﻿using Api2.Data;
using Api2.Entities;

namespace Api2.Controllers
{
    public class IndividualsController : CrudController<Individual>
    {
        public IndividualsController(MyDbContext context) : base(context)
        {

        }

    }
}