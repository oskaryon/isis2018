﻿using Api2.Data;
using Api2.Entities;

namespace Api2.Controllers
{
    public class BankAccountsController : CrudController<BankAccount>
    {
        public BankAccountsController(MyDbContext context) : base(context)
        {

        }

    }
}