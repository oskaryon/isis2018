﻿using System.Threading.Tasks;
using Api2.Data;
using Api2.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api2.Controllers
{
    public class CompanyContractsController : CrudController<CompanyContract>
    {
        public CompanyContractsController(MyDbContext context) : base(context)
        {

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override async Task<IActionResult> Create(CompanyContract companyContract)
        {
            if (ModelState.IsValid)
            {
                companyContract.BankAccount = await Context.BankAccounts.FindAsync(companyContract.BankAccountId);
                companyContract.Company = await Context.Companies.FindAsync(companyContract.CompanyId);
                await DbSet.AddRangeAsync(companyContract);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public override async Task<IActionResult> Index()
        {
            return View(await DbSet.Include("BankAccount").Include("Company").ToListAsync());
        }
    }
}