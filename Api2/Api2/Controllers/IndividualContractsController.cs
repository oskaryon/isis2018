﻿using System.Threading.Tasks;
using Api2.Data;
using Api2.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api2.Controllers
{
    public class IndividualContractsController : CrudController<IndividualContract>
    {
        public IndividualContractsController(MyDbContext context) : base(context)
        {

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override async Task<IActionResult> Create(IndividualContract individualContract)
        {
            if (ModelState.IsValid)
            {
                individualContract.BankAccount = await Context.BankAccounts.FindAsync(individualContract.BankAccountId);
                individualContract.Individual = await Context.Individuals.FindAsync(individualContract.IndividualId);
                await DbSet.AddRangeAsync(individualContract);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public override async Task<IActionResult> Index()
        {
            return View(await DbSet.Include("BankAccount").Include("Individual").ToListAsync());
        }
    }
}