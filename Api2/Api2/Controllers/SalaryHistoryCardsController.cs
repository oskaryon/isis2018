﻿using System.Threading.Tasks;
using Api2.Data;
using Api2.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api2.Controllers
{
    public class SalaryHistoryCardsController : CrudController<SalaryHistoryCard>
    {
        public SalaryHistoryCardsController(MyDbContext context) : base(context)
        {
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override async Task<IActionResult> Create(SalaryHistoryCard salaryHistoryCard)
        {
            if (ModelState.IsValid)
            {
                salaryHistoryCard.Contract = await Context.Contracts.FindAsync(salaryHistoryCard.ContractId);
                await DbSet.AddRangeAsync(salaryHistoryCard);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public override async Task<IActionResult> Index()
        {
            return View(await Context.SalaryHistoryCards.Include("Contract").ToListAsync());
        }
    }
}