﻿using System.Threading.Tasks;
using Api2.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api2.Controllers
{
    public abstract class CrudController<TEntity> : Controller where TEntity : class
    {
        protected MyDbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        protected CrudController(MyDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        [HttpGet]
        public virtual async Task<IActionResult> Index()
        {
            return View(await DbSet.ToListAsync());
        }

        [HttpGet]
        public virtual async Task<IActionResult> Details(int id)
        {
            return View(await DbSet.FindAsync(id));
        }

        [HttpGet]
        public virtual IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> Create(TEntity model)
        {
            if (ModelState.IsValid)
            {
                await DbSet.AddRangeAsync(model);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual async Task<IActionResult> Edit(int id)
        {
            return View(await DbSet.FindAsync(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual async Task<IActionResult> Edit(TEntity item)
        {
            DbSet.UpdateRange(item);
            await Context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public virtual async Task<IActionResult> Delete(int id)
        {
            DbSet.RemoveRange(await DbSet.FindAsync(id));
            await Context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}