﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api2.Data;
using Api2.Entities;
using Api2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StackExchange.Redis;

namespace Api2.Controllers
{
    public class ContractsController : CrudController<Contract>
    {
        public ContractsController(MyDbContext context) : base(context)
        {
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public override async Task<IActionResult> Create(Contract contract)
        {
            if (ModelState.IsValid)
            {
                contract.BankAccount = await Context.BankAccounts.FindAsync(contract.BankAccountId);
                await DbSet.AddRangeAsync(contract);
                await Context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public override async Task<IActionResult> Index()
        {
            return View(await Context.Contracts.Include("BankAccount").ToListAsync());
        }
    }
}