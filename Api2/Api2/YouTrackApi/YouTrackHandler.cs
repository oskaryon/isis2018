﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using YouTrackSharp;
using YouTrackSharp.Issues;

namespace Api2.YouTrackApi
{
    public class YouTrackHandler
    {
        private readonly UsernamePasswordConnection _connection;
        private readonly TimeSpan _lastPayment;

        public YouTrackHandler(YouTrackCredentials credentials)
        {
            _connection = new UsernamePasswordConnection(credentials.BaseUrl, credentials.Login, credentials.Password);
            //_lastPayment = GetLastPayment();
            _lastPayment = TimeSpan.MinValue;
        }

        private void Pay(List<Assignee> assignees, double time)
        {
            foreach (var assignee in assignees)
            {
                Console.WriteLine($"Сотруднику {assignee.FullName} выплачено за {time} часов");
            }
        }

        private static TimeSpan GetLastPayment()
        {
            throw new NotImplementedException();
        }

        public List<Tuple<List<Assignee>, double>> ProcessPayment()
        {
            var all = new List<Tuple<List<Assignee>, double>>();
            var projectrepository = _connection.CreateProjectsService();
            var projects = projectrepository.GetAccessibleProjects().Result;
            var issuerepository = _connection.CreateIssuesService();
            foreach (var x in projects)
            {
                var issues = issuerepository.GetIssuesInProject(x.ShortName).Result;
                foreach (var issue in issues)
                {
                    if (((List<string>) issue.GetField("State").Value)[0] != "Verified") break;
                    var assignees = (List<Assignee>) issue.GetField("Assignee").Value;
                    var created = TimeSpan.FromMilliseconds(Int64.Parse(issue.GetField("created").Value.ToString()));
                    var resolved = TimeSpan.FromMilliseconds(Int64.Parse(issue.GetField("resolved").Value.ToString()));
                    if (resolved < _lastPayment) break;

                    //Pay(assignees, (resolved - created).TotalHours);
                    all.Add(Tuple.Create(assignees, (resolved - created).TotalHours));
                }
            }

            return all;
        }
    }

    public class YouTrackCredentials
    {
        public readonly string BaseUrl;
        public readonly string Login;
        public readonly string Password;

        public YouTrackCredentials(IConfiguration configuration)
        {
            BaseUrl = configuration["YouTrack:baseUrl"];
            Login = configuration["YouTrack:Login"];
            Password = configuration["YouTrack:Password"];
        }
    }
}