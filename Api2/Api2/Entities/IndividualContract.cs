﻿using Api2.Models;

namespace Api2.Entities
{
    public class IndividualContract : Contract
    {
        public decimal Salary { get; set; }
        public int IndividualId { get; set; }
        public virtual Individual Individual { get; set; }
    }
}