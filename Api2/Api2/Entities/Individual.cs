﻿using Api2.Models;

namespace Api2.Entities
{
    public class Individual : BaseEntity
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SecondName { get; set; }
        public string Passport { get; set; }
        // ReSharper disable once InconsistentNaming
        public string TIN { get; set; }
    }
}