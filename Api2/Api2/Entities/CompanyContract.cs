﻿using Api2.Models;

namespace Api2.Entities
{
    public class CompanyContract : Contract
    {
        public decimal Rate { get; set; }
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}