﻿using Api2.Models;

namespace Api2.Entities
{
    public class BankAccount : BaseEntity
    {
        public string Bank { get; set; }
        // ReSharper disable once InconsistentNaming
        public string BIC { get; set; }
        public string CorrespondentAccount { get; set; }
        public string Account { get; set; }
    }
}