﻿using Api2.Models;

namespace Api2.Entities
{
    // ReSharper disable InconsistentNaming
    public class Company : BaseEntity
    {
        public string BIN { get; set; }
        public string Name { get; set; }
        public string TIN { get; set; }
        public string CPR { get; set; }
        public int CEOId { get; set; }

        public virtual Individual CEO { get; set; }
    }
}