﻿using System;
using Api2.Models;

namespace Api2.Entities
{
    // ReSharper disable InconsistentNaming
    public class SalaryHistoryCard : BaseEntity
    {
        public decimal PaidAmount { get; set; }
        public DateTime Date { get; set; }
        public int ContractId { get; set; }
        
        public Contract Contract { get; set; }
    }
}