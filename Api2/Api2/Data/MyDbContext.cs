﻿using Api2.Entities;
using Api2.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Api2.Data
{
    public class MyDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<BankAccount> BankAccounts { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyContract> CompanyContracts { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<IndividualContract> IndividualContracts { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<SalaryHistoryCard> SalaryHistoryCards { get; set; }

        public MyDbContext(DbContextOptions options) : base(options)
        {

        }

        public MyDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

    }
}
