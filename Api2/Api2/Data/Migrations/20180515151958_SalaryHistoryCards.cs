﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Api2.Data.Migrations
{
    public partial class SalaryHistoryCards : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Individuals_IndividualId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "IdividualId",
                table: "Contracts");

            migrationBuilder.AddColumn<double>(
                name: "HoursPerMonth",
                table: "Contracts",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "WorkedHours",
                table: "Contracts",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "SalaryHistoryCards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ContractId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    PaidAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SalaryHistoryCards", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SalaryHistoryCards_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SalaryHistoryCards_ContractId",
                table: "SalaryHistoryCards",
                column: "ContractId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Individuals_IndividualId",
                table: "Contracts",
                column: "IndividualId",
                principalTable: "Individuals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_Individuals_IndividualId",
                table: "Contracts");

            migrationBuilder.DropTable(
                name: "SalaryHistoryCards");

            migrationBuilder.DropColumn(
                name: "HoursPerMonth",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "WorkedHours",
                table: "Contracts");

            migrationBuilder.AddColumn<int>(
                name: "IdividualId",
                table: "Contracts",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_Individuals_IndividualId",
                table: "Contracts",
                column: "IndividualId",
                principalTable: "Individuals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
